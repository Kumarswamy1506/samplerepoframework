package com.lumity.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lumity.locators.HomePageLocators;
import com.lumity.utils.SeleniumUtils;;

public class HomePage extends HomePageLocators {

	WebElement enterPincode, searchMedicine, clickAfterMedicineEntered;

	protected void setPinCodeOnUI(String pincodetext) throws InterruptedException {
		// TODO Auto-generated method stub

		if (SeleniumUtils.isElementFound(pincode)) {
			SeleniumUtils.setValue(pincode, pincodetext);
		}

	}

	protected void searchMedicine(String medicine) throws InterruptedException {
		// TODO Auto-generated method stub

		SeleniumUtils.click(search_bar);
		SeleniumUtils.setValue(search_bar, medicine);
		SeleniumUtils.click(search_button);

	}

	public void clickForLoginPage(WebDriver driver) {

		SeleniumUtils.click(click_to_login);
	}

	

}
