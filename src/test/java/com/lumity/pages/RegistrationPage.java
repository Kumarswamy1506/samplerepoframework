package com.lumity.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.lumity.locators.RegistrationPageLocators;
import com.lumity.model.RegistrationModel;
import com.lumity.utils.SeleniumUtils;

public class RegistrationPage extends RegistrationPageLocators {

	public void enterBasicDetails(RegistrationModel registrationModel) {

		SeleniumUtils.setValue(first_name, registrationModel.getFirstName());
		SeleniumUtils.setValue(middle_name, registrationModel.getMiddleName());
		SeleniumUtils.setValue(last_name, registrationModel.getLastName());
		SeleniumUtils.setValue(email, registrationModel.getEmailAddress());
		SeleniumUtils.setValue(mobile_number, registrationModel.getMobileNumber());

	}

}
