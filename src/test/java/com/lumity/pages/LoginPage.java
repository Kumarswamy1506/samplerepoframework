package com.lumity.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.lumity.locators.LoginPageLocators;
import com.lumity.utils.SeleniumUtils;

public class LoginPage extends LoginPageLocators {

	public void enterUserDetails(String userName, String pwd) {

		SeleniumUtils.setValue(user_name, userName);
		SeleniumUtils.setValue(user_password, pwd);
		SeleniumUtils.click(login_button);

	}

}
