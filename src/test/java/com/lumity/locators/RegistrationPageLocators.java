package com.lumity.locators;

import org.openqa.selenium.By;

import com.lumity.utils.BrowserSettings;

public class RegistrationPageLocators extends BrowserSettings {

	public static final String FIRST_NAME = "//*[@id='firstname']";
	public static final String MIDDLE_NAME = "//*[@id='middlename']";
	public static final String LAST_NAME = "//*[@id='lastName']";
	public static final String EMAIL = "//*[@id='email_address']";
	public static final String MOBILE_NUMBER = "//*[@id='mobile_number']";

	public static final By first_name = By.xpath(FIRST_NAME);
	public static final By middle_name = By.xpath(MIDDLE_NAME);
	public static final By last_name = By.xpath(LAST_NAME);
	public static final By email = By.xpath(EMAIL);
	public static final By mobile_number = By.xpath(MOBILE_NUMBER);
}
