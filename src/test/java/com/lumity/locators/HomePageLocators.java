package com.lumity.locators;

import org.openqa.selenium.By;

import com.lumity.utils.BrowserSettings;

public class HomePageLocators extends BrowserSettings {

	public static final String PINCODE = "//input[@id='autocomplete']";
	public static final String SEARCH_BAR = "//input[@id='search']";
	public static final String SEARCH_BUTTON = "//button[@type='submit']";
	public static final String CLICK_TO_LOGIN = "//*[@href='https://medleymed.com/customer/account/login/']";

	public static final By pincode = By.xpath(PINCODE);
	public static final By search_bar = By.xpath(SEARCH_BAR);
	public static final By search_button = By.xpath(SEARCH_BUTTON);
	public static final By click_to_login = By.xpath(CLICK_TO_LOGIN);

}
