package com.lumity.locators;

import org.openqa.selenium.By;

import com.lumity.utils.BrowserSettings;

public class LoginPageLocators extends BrowserSettings {

	public static final String USER_NAME = "//*[@name='login[username]']";
	public static final String PASSWORD = "//*[@name='login[password]']";
	public static final String LOGIN_BUTTON = "//*[@title='Login']";

	public static final By user_name = By.xpath(USER_NAME);
	public static final By user_password = By.xpath(PASSWORD);
	public static final By login_button = By.xpath(LOGIN_BUTTON);
}
