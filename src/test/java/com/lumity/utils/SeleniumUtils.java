package com.lumity.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumUtils {

	private static WebDriver webDriver;
	public static String configFilePath = "D:\\eclipse_workspaces\\SampleMaven\\src\\test\\java\\com\\lumity\\utils\\Config.properties";

	// Class variable to read the configurations & the test data..
	static Properties prop;

	// Create a string variable to capture the exception message..
	public static String exceptionMsg = "";

	// static variable to define the timeout period..
	final static int timeout = 60;

	public static void setDriver(WebDriver driver) {
		webDriver = driver;
	}

	public static WebDriver getDriver() {
		return webDriver;
	}

	// Method to check whether the element is found..
	public static boolean isElementFound(By locator) {
		try {
			WebDriverWait wait = new WebDriverWait(SeleniumUtils.getDriver(), timeout);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			return true;
		} catch (WebDriverException ex) {
			exceptionMsg = locator + " - element not found | element not visible!! " + ex.getMessage();
			return false;
		}
	}

	// Method to accept the Alert Pop up..
	public static void clickAlertPopUp() {
		try {
			WebDriverWait wait = new WebDriverWait(SeleniumUtils.getDriver(), timeout);
			if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
				// Wait for Alert to be present
				Alert myAlert = SeleniumUtils.getDriver().switchTo().alert();
				myAlert.accept();
			}
		} catch (WebDriverException e) {
			exceptionMsg = "AlertPopUp not found | AlertPopUp not visible!! " + e.getMessage();

		}
	}

	// Method to read the test data as key and values from the text file..
	public static Properties readFile(String path) throws IOException {
		Properties prop = new Properties();
		FileInputStream inputFile = null;

		// Read the file for the path specified..
		try {
			inputFile = new FileInputStream(path);
		} catch (FileNotFoundException e) {

			System.out.println("Exception thrown for filepath: " + path + " - " + e.getMessage());
			System.out.println("Stack Trace follows: ");
			e.printStackTrace();
		}

		// Get the properties from the file path specified..
		try {
			prop.load(inputFile);
		} catch (Exception e) {

			System.out.println("Exception thrown for filepath: " + path + " - " + e.getMessage());
			System.out.println("Stack Trace follows: ");
			e.printStackTrace();
		}

		return prop;
	}

	// Method to clear the values in the fields..
	public static void clearValues(By locator) {
		try {
			WebElement element = SeleniumUtils.getDriver().findElement(locator);
			element.clear();
		} catch (Exception ex) {
			System.out.println(
					"Text field element is not visible | not enable to clear the values - Error: " + ex.getMessage());
		}
	}

	// Method to set the values into the text fields..
	public static void setValue(By locator, String value) {
		try {
			WebElement element = SeleniumUtils.getDriver().findElement(locator);
			element.sendKeys(value);
		} catch (Exception ex) {
			System.out.println(
					"Text field element is not visible | not enabled to enter the values - Error: " + ex.getMessage());
		}
	}

	// Method to click the element..
	public static void click(By locator) {
		try {
			WebElement element = SeleniumUtils.getDriver().findElement(locator);
			element.click();
		} catch (Exception ex) {
			System.out.println("Element is not visible | not clickable - Error: " + ex.getMessage());
		}
	}

	// Method to get text of the element..
	public static String getText(By locator) {
		return SeleniumUtils.getDriver().findElement(locator).getText();
	}

	// Method to get value of a locator on the page..
	public static String getSubText(By locator) {
		return SeleniumUtils.getDriver().findElement(locator).getAttribute("value");
	}

	// Method to find the element
	public static WebElement findElement(By Locator) {

		return SeleniumUtils.getDriver().findElement(Locator);
	}

}
