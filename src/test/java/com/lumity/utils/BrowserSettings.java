package com.lumity.utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BrowserSettings extends BaseExtentReports {

	public WebDriver driver;
	String baseURL;
	

	public WebDriver getDriver() {
		return driver;
	}

	public void launchApp(String appURL) {

		try {
			setDriver(appURL);

		} catch (Exception e) {
			System.out.println("Error....." + e.getStackTrace());
		}

	}

	public void closeApp() {

		driver.close();

	}

	private void setDriver(String appURL) {

		String browserType = new ReadFileData().getDataFromPrpertiesFile(SeleniumUtils.configFilePath).getProperty("browser");
		baseURL = new ReadFileData().getDataFromPrpertiesFile(SeleniumUtils.configFilePath).getProperty("baseURL");
		System.out.println(baseURL);
		if (browserType.equalsIgnoreCase("chrome")) {
			driver = initChromeDriver(baseURL + "/" + appURL);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} else if (browserType.equalsIgnoreCase("firefox")) {
			driver = initFirefoxDriver(baseURL + "/" + appURL);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} else {
			driver = initFirefoxDriver(baseURL + "/" + appURL);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

		SeleniumUtils.setDriver(driver);
	}

	private WebDriver initChromeDriver(String appURL) {
		System.out.println("Launching google chrome with new profile..");
		System.setProperty("webdriver.chrome.driver", "D:\\ChromeDriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(appURL);
		return driver;
	}

	private WebDriver initFirefoxDriver(String appURL) {
		System.out.println("Launching Firefox browser..");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to(appURL);
		return driver;
	}

}
