package com.lumity.testcases;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.lumity.pages.HomePage;
import com.lumity.pages.LoginPage;

public class LoginValidation extends LoginPage {

	@BeforeClass
	public void setUp() {
		launchApp("");
	}

	@Test
	public void loginValidationTest() throws InterruptedException {

		test = extent.createTest("loginValidationTest");
		HomePage homepage = new HomePage();
		homepage.clickForLoginPage(driver);
		enterUserDetails("qwreqwr@gmail.com", "13241edasf");
		Thread.sleep(6000);
		String actual = driver.getCurrentUrl();
		///Assert.assertEquals(actual, "https://medleymed.com/");
		Assert.assertTrue(false);

	}

	@AfterClass
	public void closeApp() {
		driver.close();
	}

}
